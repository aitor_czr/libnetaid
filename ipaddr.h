 /*
  * ipaddr.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * Most of the code of this file is taken from the RTnetlink service routines
  * of the iproute2 project.
  * 
  * https://git.kernel.org/pub/scm/network/iproute2/iproute2.git/
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */

#ifndef __IPADDR_H__
#define __IPADDR_H__

#include <linux/netlink.h>
#include <linux/rtnetlink.h> 

#define     MAX_FLUSH_LOOPS     10
#define     INFINITY_LIFE_TIME  0xFFFFFFFFU

int flush_update(void);
void ipaddr_flush(const char*);
int print_addrinfo( const struct sockaddr_nl *who,
					struct nlmsghdr *n,
					void *arg );
					
int print_addrinfo_primary( const struct sockaddr_nl *who,
								   struct nlmsghdr *n, void *arg );
								   
int print_addrinfo_secondary( const struct sockaddr_nl *who,
									 struct nlmsghdr *n,
									 void *arg );					
					
unsigned int get_ifa_flags( struct ifaddrmsg *ifa,
								   struct rtattr *ifa_flags_attr );

#endif /* __IPADDR_H__ */
