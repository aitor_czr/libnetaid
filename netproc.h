/*
 * netproc.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * Most of the code of this file is taken from the netstat plug-in of the Lxpanel, 
 * developed by the LxDE team.
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */
 
#ifndef __NETPROC_H__
#define __NETPROC_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
//#include <sys/un.h>   // for struct sockaddr_un
#include <netinet/in.h>
#include <net/if.h>
#include <net/if_arp.h>
#include <arpa/inet.h>
#include <linux/sockios.h>
#include <linux/ethtool.h>

// Forward declaration:
struct sbuf;

#define INTSIZE 20
#define BUFFER_SIZE 512

#define TRUE 1
#define FALSE 0

typedef struct {
	char *ifname;
	char *mac;
	char *ipaddr;
	char *dest;
	char *bcast;
	char *mask;
	char *type;
	int flags;
	bool alive;
	bool enable;
	bool updated;
	bool plug;
	bool connected;

	/* wireless */
	bool wireless;
	char *protocol;
	char *essid;
	int quality;

	int status;
	ulong recv_bytes;
	ulong recv_packets;
	ulong trans_bytes;
	ulong trans_packets;
	
} netdevice;

typedef struct netdevice_node {
	netdevice             info;
	struct netdevice_node *prev;
	struct netdevice_node *next;
	
} NETDEVLIST;

typedef NETDEVLIST *NETDEVLIST_PTR;

void
netproc (struct sbuf*);

FILE*
netproc_open (void);

void
netproc_close (FILE*);

bool
wireless_refresh (
	int,
	const char*
);
							
void
netproc_netdevlist_add (
	NETDEVLIST_PTR*, 
	const char*,
	ulong,
	ulong,
	ulong,
	ulong,
	bool
);
                                   
void
netproc_netdevlist_destroy( NETDEVLIST_PTR );

void
netproc_netdevlist_clear ( NETDEVLIST_PTR* );

NETDEVLIST_PTR
netproc_netdevlist_find (
	NETDEVLIST_PTR,
	const char*
);
												
char*
netproc_parse_ifname ( const char* );

void
netproc_parse_stats_header (
	char*,
	int*,
	int*,
	int*,
	int*
);
                                       

bool netproc_parse_status (
	char*,
	int,
	int,
	ulong*,
	ulong*,
	int,
	int,
	ulong*,
	ulong*
);

char *itoa__(int);

#endif /* __NETPROC_H__ */
