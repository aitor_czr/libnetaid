 /*
  * iproute.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * Most of the code of this file is taken from the RTnetlink service routines
  * of the iproute2 project.
  * 
  * https://git.kernel.org/pub/scm/network/iproute2/iproute2.git/
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */

#include <stdbool.h>  
#include "iproute.h"
#include "ll_map.h"
#include "def.h"

#define IFNAME_SIZ 64

static struct
{
	unsigned int tb;
	int cloned;
	char *flushb;
	int oif, oifmask;
	inet_prefix rvia;
	inet_prefix mdst;
	inet_prefix msrc;
} filter;

static char ifname[IFNAME_SIZ];	
static struct rtnl_handle rtnl_h = { .fd = -1 };

static
int af_bit_len(int af)
{
   switch (af) {
   case AF_INET6:
      return 128;
   case AF_INET:
      return 32;
   case AF_DECnet:
      return 16;
   case AF_IPX:
      return 80;
   default:
      return 20;
   }

   return 0;
}

char* iproute()
{	
	if (rtnl_open(&rtnl_h, 0) < 0) exit(1);
	
	int do_ipv6 = AF_UNSPEC;	
	rtnl_filter_t filter_fn;
	
	memset(&ifname, 0, IFNAME_SIZ);
		
	filter_fn = (rtnl_filter_t)print_route;
	
	memset(&filter, 0, sizeof(filter));
	filter.mdst.bitlen = -1;
	filter.msrc.bitlen = -1;
	filter.oif = 0;
	if (filter.oif > 0)
		filter.oifmask = -1;
	filter.tb = RT_TABLE_MAIN;
	
	if (filter.tb) do_ipv6 = AF_INET;
	
	if (!filter.cloned) {
		if (rtnl_wilddump_request(&rtnl_h, do_ipv6, RTM_GETROUTE) < 0) {
			perror(_("Cannot send dump request"));
			rtnl_close(&rtnl_h);
			exit(1);
		}
	} else {
		if (rtnl_rtcache_request(&rtnl_h, do_ipv6) < 0) {
			perror(_("Cannot send dump request"));
			rtnl_close(&rtnl_h);
			exit(1);
		}
	}

	if (rtnl_dump_filter_nc(&rtnl_h, filter_fn, stdout, 0) < 0) {
		fprintf(stderr, _("Dump terminated\n"));
		rtnl_close(&rtnl_h);
		exit(1);
	}

	rtnl_close(&rtnl_h);
	
	free(filter.flushb);

	return ifname;
}

unsigned int print_route(const struct sockaddr_nl *who, struct nlmsghdr *n)
{
	struct rtmsg *r = NLMSG_DATA(n);
	int len = n->nlmsg_len;
	struct rtattr *tb[RTA_MAX+1];
	int host_len;
		
	host_len = af_bit_len(r->rtm_family);
	
	if (n->nlmsg_type != RTM_NEWROUTE && n->nlmsg_type != RTM_DELROUTE) {
		fprintf(stderr, _("Not a route: %08x %08x %08x\n"),
			n->nlmsg_len, n->nlmsg_type, n->nlmsg_flags);
		return -1;
	}
	if (filter.flushb && n->nlmsg_type != RTM_NEWROUTE)
		return 0;
		
	len -= NLMSG_LENGTH(sizeof(*r));
	if (len < 0) {
		fprintf(stderr, _("BUG: wrong nlmsg len %d\n"), len);
		return -1;
	}
	
    switch(r->rtm_flags) {
       case RTNH_F_DEAD:
       case RTNH_F_PERVASIVE:
       case RTNH_F_OFFLOAD:
       case RTM_F_NOTIFY:
       case RTNH_F_LINKDOWN:
       case RTNH_F_UNRESOLVED:
       case RTM_F_OFFLOAD:
       case RTM_F_TRAP:
          break;
       case RTNH_F_ONLINK:
       default: 
          { 
	          parse_rtattr(tb, RTA_MAX, RTM_RTA(r), len);
	
              /*  RTA_IIF is the INPUT INTERFACE INDEX
 	           *  RTA_OIF is the OUTPUT INTERFACE INDEX
	           *  For more details, see the manpages of netlink:
               *  http://man7.org/linux/man-pages/man7/rtnetlink.7.html     
               * */
	          
	          if (tb[RTA_OIF] && filter.oifmask != -1 && (tb[RTA_GATEWAY] && filter.rvia.bitlen != host_len)) {
                 strcpy(ifname, ll_index_to_name(*(int *)RTA_DATA(tb[RTA_OIF])));
              } 
          }
          break;
    }

	return r->rtm_flags;
}

int rtnl_rtcache_request(struct rtnl_handle *rtnl_h, int family)
{
	struct {
		struct nlmsghdr nlh;
		struct rtmsg rtm;
	} req = {
		.nlh.nlmsg_len = sizeof(req),
		.nlh.nlmsg_type = RTM_GETROUTE,
		.nlh.nlmsg_flags = NLM_F_ROOT | NLM_F_REQUEST,
		.nlh.nlmsg_seq = rtnl_h->dump = ++rtnl_h->seq,
		.rtm.rtm_family = family,
		.rtm.rtm_flags = RTM_F_CLONED,
	};
	struct sockaddr_nl nladdr = { .nl_family = AF_NETLINK };

	return sendto(rtnl_h->fd, (void *)&req, sizeof(req), 0, (struct sockaddr *)&nladdr, sizeof(nladdr));
}
