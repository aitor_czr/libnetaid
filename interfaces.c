/*
 * interfaces.c
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include "interfaces.h"
#include "iproute.h"
#include "sbuf.h"
#include "def.h"

#include <errno.h>
#include <iwlib.h>
#include <assert.h>

// Usage: strncpy_t(str1, sizeof(str1), str2, strlen(str2));
// Copy string "in" with at most "insz" chars to buffer "out", which
// is "outsz" bytes long. The output is always 0-terminated. Unlike
// strncpy(), strncpy_t() does not zero fill remaining space in the
// output buffer:
static
char* strncpy_t(char* out, size_t outsz, const char* in, size_t insz)
{
   assert(outsz > 0);
   while(--outsz > 0 && insz > 0 && *in) { *out++ = *in++; insz--; }
   *out = 0;
   return out;
}

static
short exists(char *file)
{
	int fd=open(file, O_RDONLY);
	if (fd<0) return (errno==ENOENT)?-1:-2;
	close(fd);
	return 0;
}

void get_interfaces(struct sbuf* wired_interface, struct sbuf* wireless_interface)
{
    struct if_nameindex *if_nidxs, *intf;

    if_nidxs = if_nameindex();
    if ( if_nidxs != NULL )
    {
        for (intf = if_nidxs; intf->if_index != 0 || intf->if_name != NULL; intf++)
        {
            struct sbuf fp;
            sbuf_init(&fp);
            sbuf_concat(&fp, 3, "/sys/class/net/", intf->if_name, "/device/uevent");
            
            int f=exists(fp.buf);
			switch (f)
			{
				case 0:  {							
							struct stat st;
							struct sbuf dir;
							
							sbuf_init(&dir);
							sbuf_concat(&dir, 3, "/sys/class/net/", intf->if_name, "/wireless");
							
							if(stat(dir.buf, &st) == -1) sbuf_addstr(wired_interface, intf->if_name);
							else if(st.st_mode & S_IFDIR) sbuf_addstr(wireless_interface, intf->if_name);							
							free(dir.buf);
						 }
						 
						 break;
				
				case -1: break;
				
				case -2: printf (_("Enable to open %d (%s)\n"), errno, strerror(errno));
						 break;
				
				default: break;
			}		
			sbuf_free(&fp);
        }

        if_freenameindex(if_nidxs);
    }
}

void interface_up (const char *if_name)
{
    struct ifreq ifr;
    int skfd;	  /* generic raw socket desc. */

	/* Create a channel to the NET kernel. */
	if((skfd = iw_sockets_open()) < 0)
    {
		perror(_("Enable to create a channel to the NET kernel."));
		exit(-1);
    }

    strncpy_t(ifr.ifr_name, IFNAMSIZ, if_name, strlen(if_name));

    if (ioctl(skfd, SIOCGIFFLAGS, &ifr) >= 0) {     
        strncpy_t(ifr.ifr_name, IFNAMSIZ, if_name, strlen(if_name));
        ifr.ifr_flags |= (IFF_UP | IFF_RUNNING);
        ioctl(skfd, SIOCSIFFLAGS, &ifr);
    } else {
        log_error("\nGetting flags for interface %s failed, not activating interface.\n", if_name);
    }
    
    /* Close the socket. */
	iw_sockets_close(skfd);
}

void interface_down (const char *if_name)
{
    struct ifreq ifr;
    int skfd;	  /* generic raw socket desc. */

	/* Create a channel to the NET kernel. */
	if((skfd = iw_sockets_open()) < 0)
    {
		perror(_("\nEnable to create a channel to the NET kernel.\n"));
		exit(-1);
    }

    strncpy_t(ifr.ifr_name, IFNAMSIZ, if_name, strlen(if_name));

    if (ioctl(skfd, SIOCGIFFLAGS, &ifr) >= 0) {
        ifr.ifr_flags &= ~IFF_UP;
        ioctl(skfd, SIOCSIFFLAGS, &ifr);
    } else {
        log_error("\nGetting flags for interface %s failed, not taking down interface.\n", if_name);
    }
    
    /* Close the socket. */
	iw_sockets_close(skfd);
}

bool get_interface_status(const char *if_name)
{
	bool is_up = false;
    struct ifreq ifr;
    int skfd;	  /* generic raw socket desc. */

	/* Create a channel to the NET kernel. */
	if((skfd = iw_sockets_open()) < 0)
    {
		perror(_("\nEnable to create a channel to the NET kernel.\n"));
		return(-1);
    }
	
	strncpy_t(ifr.ifr_name, IFNAMSIZ, if_name, strlen(if_name));

	if (ioctl(skfd, SIOCGIFFLAGS, &ifr) >= 0) {
		if (ifr.ifr_flags&IFF_UP) is_up = true;
    } else is_up = false;
    
    /* Close the socket. */
	iw_sockets_close(skfd);
	
	return is_up;
}   
