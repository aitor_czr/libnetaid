 /*
  * ethlink.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */

#include "ethlink.h"
#include "def.h"

#define ETHTOOL_GLINK        0x0000000a /* Get link status (ethtool_value) */
 
typedef signed int u32;
 
/* for passing single values */
struct ethtool_value
{
    u32    cmd;
    u32    data;
};
 
interface_status_t interface_detect_beat_ethtool(int fd, const char *iface)
{
    struct ifreq ifr;
    struct ethtool_value edata;
   
    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, iface, sizeof(ifr.ifr_name)-1);
 
    edata.cmd = ETHTOOL_GLINK;
    ifr.ifr_data = (caddr_t) &edata;
 
    if (ioctl(fd, SIOCETHTOOL, &ifr) == -1)
    {
        perror(_("ETHTOOL_GLINK failed "));
        return IFSTATUS_ERR;
    }
 
    return edata.data ? IFSTATUS_UP : IFSTATUS_DOWN;
}
 
int ethlink(const char* ifname)
{
    interface_status_t status;
    int fd, res;
    
    if((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket ");
        exit(0);
    }
    
    status = interface_detect_beat_ethtool(fd, ifname);
    close(fd);
 
    switch (status)
    {
        case IFSTATUS_UP:
            res=1;
            break;
        
        case IFSTATUS_DOWN:
            res=0;
            break;
        
        default:
            res=-1;
            break;
    }
 
    return res;
}
