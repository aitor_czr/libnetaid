/*
  * iproute.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * Most of the code of this file is taken from the RTnetlink service routines
  * of the iproute2 project.
  * 
  * https://git.kernel.org/pub/scm/network/iproute2/iproute2.git/
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */
  
#ifndef __IPROUTE_H__
#define __IPROUTE_H__
  
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <linux/in_route.h>
#include <linux/icmpv6.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>  
#include <stdbool.h>
#include <math.h>

#include "libnetlink.h"

typedef struct
{
	__u16 flags;
	__u16 bytelen;
	__s16 bitlen;
	/* These next two fields match rtvia */
	__u16 family;
	__u32 data[8];
} inet_prefix;

char* iproute();
unsigned int print_route(const struct sockaddr_nl*, struct nlmsghdr*);
int rtnl_rtcache_request(struct rtnl_handle*, int);

#endif /* __IPROUTE_H__ */

