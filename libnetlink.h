 /*
  * libnetlink.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * Most of the code of this file is taken from the RTnetlink service routines
  * of the iproute2 project.
  * 
  * https://git.kernel.org/pub/scm/network/iproute2/iproute2.git/
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */

#ifndef __LIBNETLINK_H__
#define __LIBNETLINK_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <unistd.h>

struct rtnl_handle {
	int					fd, proto, flags;
	FILE		        *dump_fp;
	struct sockaddr_nl	local, peer;
	__u32				seq, dump;
};

typedef int (*rtnl_filter_t)( const struct sockaddr_nl *,
							  struct nlmsghdr *n,
							  void * );

struct rtnl_dump_filter_arg {
	rtnl_filter_t filter;
	void *arg1;
	__u16 nc_flags;
};

void rtnl_close( struct rtnl_handle *rth );

int rtnl_open_byproto( struct rtnl_handle *rth, 
					   unsigned int subscriptions,
					   int protocol );
					   	
int rtnl_open( struct rtnl_handle *rth, 
			   unsigned int subscriptions );
			   	
int rtnl_dump_filter_l( struct rtnl_handle *rth,
						const struct rtnl_dump_filter_arg *arg );
							
int rtnl_dump_filter_nc( struct rtnl_handle *rth,
						 rtnl_filter_t filter,
						 void *arg1, __u16 nc_flags );
						 	
int rtnl_wilddump_req_filter( struct rtnl_handle *rth, 
							  int family, 
							  int type, 
							  __u32 filt_mask );
							  	
int rtnl_wilddump_request( struct rtnl_handle *rth, 
						   int family, 
						   int type );
						   	
int rtnl_send_check (struct rtnl_handle *rth, const void *buf, int )

__attribute__((warn_unused_result));

int parse_rtattr( struct rtattr *tb[], int max, struct rtattr *rta, int len );

int parse_rtattr_flags( struct rtattr *tb[], 
						int max, 
						struct rtattr *rta, 
						int len, 
						unsigned short flags );
						
static inline __u32 rta_getattr_u32(const struct rtattr *rta)
{
	return *(__u32 *)RTA_DATA(rta);
}

#endif /* __LIBNETLINK_H__ */
