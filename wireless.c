/*
 * wireless.c
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * Most of the code of this file is taken from the Wireless Tools project
 *
 *      Jean II - HPLB '99 - HPL 99->07
 *  
 * released under the GPL license.
 *     Copyright (c) 1997-2007 Jean Tourrilhes <jt@hpl.hp.com>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#include "wireless.h"
#include "sbuf.h"
#include "def.h"

#include <iwlib.h>

static inline char *
_iw_saether_ntop(const struct sockaddr*, char*);

int scan_active_wifis(const char *ifname, struct sbuf *s) 
{
   int skfd;     /* generic raw socket desc. */

   /* Create a channel to the NET kernel. */
   if((skfd = iw_sockets_open()) < 0)
    {
      perror(_("Enable to create a channel to the NET kernel."));
      exit( EXIT_FAILURE );
    }
 
   int count=print_scanning_info(skfd, ifname, s);

   /* Close the socket. */
   iw_sockets_close(skfd);
   
   return count;
}

int print_scanning_info(int   skfd,
               const char *ifname,
               struct sbuf *s)
{   
   int count = 0;
   struct iwreq wrq;
   struct iw_scan_req scanopt;      /* Options for 'set' */
   unsigned char *buffer = NULL;      /* Results */
   int buflen = IW_SCAN_MAX_DATA;    /* Min for compat WE<17 */
   struct iw_range range;
   int has_range;
   struct timeval tv;            /* Select timeout */
   int timeout = 15000000;          /* 15s */
   
   /* Get range stuff */
   has_range = (iw_get_range_info(skfd, ifname, &range) >= 0);

   /* Check if the interface could support scanning; otherwise, return doing nothing. */
   if((!has_range) || (range.we_version_compiled < 14)) return(-1);

   /* Init timeout value -> 250ms between set and first get */
   tv.tv_sec = 0;
   tv.tv_usec = 0; //250000;

   /* Clean up set args */
   memset(&scanopt, 0, sizeof(scanopt));
   wrq.u.data.pointer = (caddr_t) &scanopt;
   wrq.u.data.length = sizeof(scanopt);
   wrq.u.data.flags = 0;
    
    
   /* Initiate Scanning */
   if(iw_set_ext(skfd, ifname, SIOCSIWSCAN, &wrq) < 0)
   {
      if((errno != EPERM))
       {
         //fprintf(stderr, "%-8.16s  Interface doesn't support scanning : %s\n\n", ifname, strerror(errno));
         return(-1);
       }
       
      /* If we don't have the permission to initiate the scan, we may
       * still have permission to read left-over results.
       * But, don't wait !!! */
      tv.tv_usec = 0;
   }
 
   timeout -= tv.tv_usec;

   while(1)
    {
      fd_set rfds;      /* File descriptors for select */
      int last_fd;       /* Last fd */
      int ret;

      /* Guess what ? We must re-generate rfds each time */
      FD_ZERO(&rfds);
      last_fd = -1;

      /* In here, add the rtnetlink fd in the list */
      /* Wait until something happens */
      ret = select(last_fd + 1, &rfds, NULL, NULL, &tv);

      /* Check if there was an error */
      if(ret < 0)
      {
         if(errno == EAGAIN || errno == EINTR)
         continue;
         fprintf(stderr, _("Unhandled signal - exiting...\n"));
         return(-1);
      }

      /* Check if there was a timeout */
      if(ret == 0)
      {
         unsigned char *   newbuf;

         realloc:  /* (Re)allocate the buffer - realloc(NULL, len) == malloc(len) */
         newbuf = realloc(buffer, buflen);
         if(newbuf == NULL)
         {
            if(buffer) free(buffer);
            fprintf(stderr, _("%s: Memory allocation failed\n"), __FUNCTION__);
            return(-1);
         }
         buffer = newbuf;

         /* Try to read the results */
         wrq.u.data.pointer = buffer;
         wrq.u.data.flags = 0;
         wrq.u.data.length = buflen;
         if(iw_get_ext(skfd, ifname, SIOCGIWSCAN, &wrq) < 0)
         {
            /* Check if buffer was too small (WE-17 only) */
            if((errno == E2BIG) && (range.we_version_compiled > 16) && (buflen < 0xFFFF))
            {

               /* Check if the driver gave us any hints. */
               if(wrq.u.data.length > buflen) buflen = wrq.u.data.length;
               else buflen *= 2;

               /* wrq.u.data.length is 16 bits so max size is 65535 */
               if(buflen > 0xFFFF) buflen = 0xFFFF;

               /* Try again */
               goto realloc;
            }

            /* Check if results not available yet */
            if(errno == EAGAIN)
            {
               /* Restart timer for only 100ms*/
               tv.tv_sec = 0;
               tv.tv_usec = 100000;
               timeout -= tv.tv_usec;
               if(timeout > 0) continue;   /* Try again later */
            }

            /* Bad error */
            free(buffer);
            fprintf(stderr, _("%-8.16s  Failed to read scan data : %s\n\n"), ifname, strerror(errno));
            return(-2);
         }
       
         else break;  /* We have the results, go to process them */
       
      } // if(ret == 0)
    } // while   
   
   if(wrq.u.data.length)
    {
      struct iw_event   iwe;
      struct stream_descr   stream;
      struct iwscan_state   state = { .ap_num = 1, .val_index = 0 };
      struct sbuf a, e, q;
      int ret;
        
      iw_init_event_stream(&stream, (char *)buffer, wrq.u.data.length);
      
      int i=0;
      
      do
      {
         /* Extract an event and print it */
         ret = iw_extract_event_stream(&stream, &iwe, range.we_version_compiled);
         if(ret > 0)
         { 
            char buffer[128];   /* Temporary buffer */
            
               /* Now, let's decode the event */
               switch(iwe.cmd)
               {
                  case SIOCGIWAP:
                     sbuf_init(&a);
                     sbuf_addstr(&a, _iw_saether_ntop(&iwe.u.ap_addr, buffer));
                     count++;          
                     state.ap_num++;
                     i++;
                     break;
    
                  case SIOCGIWESSID:
                     sbuf_init(&e);
                     char essid[4*IW_ESSID_MAX_SIZE+1];
                     memset(essid, '\0', 4*IW_ESSID_MAX_SIZE+1);
                     if((iwe.u.essid.pointer) && (iwe.u.essid.length))
                     _iw_essid_escape(essid, iwe.u.essid.pointer, iwe.u.essid.length);
                     if(iwe.u.essid.flags)
                     {
                        /* Does it have an ESSID index ? */
                        if((iwe.u.essid.flags & IW_ENCODE_INDEX) > 1)
                        {
                           char w[256];
                           sprintf(w, "\"%s\" [%d]", essid, (iwe.u.essid.flags & IW_ENCODE_INDEX));
                           sbuf_addstr(&e, w);                  
                        } else {
                           if(sizeof(essid)>1) sbuf_addstr(&e, essid);
                           else sbuf_addstr(&e, "<hidden>");
                        }
                     } else sbuf_addstr(&e, "off/any");
                     
                     i++;
                     break;
    
                  case IWEVQUAL:
                     sbuf_init(&q);
                     _iw_print_stats(buffer, sizeof(buffer), &iwe.u.qual, &range, has_range, &q);
                     i++;                     
                     break;
      
                  default: break;
               } // switch
            
         } // if
            
         if(i==3) {
            i=0;
            if(s->len > 1) sbuf_addch(s, '|');
            sbuf_concat(s, 2, a.buf, "|");
            sbuf_concat(s, 2, e.buf, "|");
            sbuf_addstr(s, q.buf);
         }
            
            
      } while(ret > 0);
            
      free(a.buf);
      free(e.buf);
      free(q.buf);
      
    } else printf(_("No scan results\n\n")); 
      
    free(buffer);
    return(count);
}

void
_iw_print_stats(char *buffer,
            int   buflen,
            const iwqual *qual,
            const iwrange *range,
            int   has_range,
            struct sbuf *q)
{
   int   len;

   if(has_range && ((qual->level != 0) || (qual->updated & (IW_QUAL_DBM | IW_QUAL_RCPI))))
    {
      /* Deal with quality : always a relative value */
      if(!(qual->updated & IW_QUAL_QUAL_INVALID))
      {
         len = snprintf(buffer, buflen, "%d/%d", qual->qual, range->max_qual.qual);
         sbuf_addstr(q, buffer);
         buffer += len;
         buflen -= len;
      }
    }
    
    //sbuf_addstr(q, buffer);
}

/*
 * Display an Ethernet Socket Address in readable format.
 */
static inline char *
_iw_saether_ntop(const struct sockaddr *sap, char* bufp)
{
   //_iw_saether_ntop(&event->u.ap_addr, buffer)
   iw_ether_ntop((const struct ether_addr *) sap->sa_data, bufp);
   return bufp;
}

/*------------------------------------------------------------------*/
/*
 * Escape non-ASCII characters from ESSID.
 * This allow us to display those weirds characters to the user.
 *
 * Source is 32 bytes max.
 * Destination buffer needs to be at least 129 bytes, will be NUL
 * terminated.
 */
void
_iw_essid_escape(char *dest, const char *src, const int slen)
{
   const unsigned char *s = (const unsigned char*) src;
   const unsigned char *e = s + slen;
   char *d = dest;

   /* Look every character of the string */
   while(s < e) {
      int   isescape;
      
      /* Escape the escape to avoid ambiguity.
       * We do a fast path test for performance reason. Compiler will
       * optimise all that ;-) */
      if(*s == '\\') {
         /* Check if we would confuse it with an escape sequence */
         if((e-s) > 4 && (s[1] == 'x') && (isxdigit(s[2])) && (isxdigit(s[3]))) 
            isescape=1;
         else isescape=0;
      } else isescape=0;
      
      /* Is it a non-ASCII character ??? */
      if(isescape || !isascii(*s) || iscntrl(*s)) {
         /* Escape */
         sprintf(d, "\\x%02X", *s);
         d+=4;
      } else {
         /* Plain ASCII, just copy */
         *d=*s;
         d++;
      }
      
      s++;
    }
    
    /* NUL terminate destination */
    *d='\0';
}




