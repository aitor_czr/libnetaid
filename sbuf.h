/*
 * sbuf - Simple C string buffer implementation.
 * 
 * Forked from jgmenu/blob/master/src/sbuf.h
 * (original from https://github.com/johanmalm/jgmenu/blob/master/src/sbuf.h)
 * 
 * All modifications to the original source file are:
 * Copyright (C) 2021 Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 *
 * Original copyright and license text produced below.
 */

/*
 * sbuf.h
 * Simple C string buffer implementation.
 *
 * Copyright Johan Malm 2016
 *
 * The buffer is a byte array which is at least (len + 1) bytes in size.
 * The sbuf functions are designed too maintain/add '\0' at the end,
 * allowing the buffer to be used be used as a C string
 * (i.e. buf[len] == 0).
 *
 * sbuf_init allocates one byte so that the buffer can always be safely:
 *	- assumed to be a valid C string (i.e. buf != NULL)
 *	- freed (i.e. buf must not to point to memory on the stack)
 *
 * Example life cycle:
 *	sbuf_t s;
 *	sbuf_init(&s);
 *	sbuf_addch(&s, 'F');
 *	sbuf_addstr(&s, "oo");
 *	printf("%s\n", s.buf);
 *	free(s.buf);
 */

#ifndef __SBUF_H__
#define __SBUF_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define __cleanbuf__  __attribute__((cleanup(sbuf_free)))

typedef struct sbuf {
	char *buf;
	int bufsiz;
	int len;
} sbuf_t;

void sbuf_init(sbuf_t*);
void sbuf_addch(sbuf_t*, char);
void sbuf_addstr(sbuf_t*, const char*);
void sbuf_concat(sbuf_t*, int, ...);
void sbuf_free(sbuf_t*);
void sbuf_random(sbuf_t*, const int);

#endif /* __SBUF_H__ */
