/*
 * wireless.h
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org> * 
 * 
 * Most of the code of this file is taken from the Wireless Tools project
 *
 *		Jean II - HPLB '99 - HPL 99->07
 *  
 * released under the GPL license.
 *     Copyright (c) 1997-2007 Jean Tourrilhes <jt@hpl.hp.com>
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file. * 
 */

#ifndef __WIRELESS_H__
#define __WIRELESS_H__

#include <iwlib.h>

// Forward declaration:
struct sbuf;

/* Paths */
#define PROC_NET_WIRELESS	"/proc/net/wireless"
#define PROC_NET_DEV		"/proc/net/dev"

#ifndef IW_EV_LCP_PK2_LEN

struct iw_pk_event
{
	__u16		len;			/* Real lenght of this stuff */
	__u16		cmd;			/* Wireless IOCTL */
	union iwreq_data	u;		/* IOCTL fixed payload */
} __attribute__ ((packed));

struct	iw_pk_point
{
  void __user	*pointer;	/* Pointer to the data  (in user space) */
  __u16		length;		/* number of fields or size in bytes */
  __u16		flags;		/* Optional params */
} __attribute__ ((packed));

#define IW_EV_LCP_PK2_LEN	(sizeof(struct iw_pk_event) - sizeof(union iwreq_data))
#define IW_EV_POINT_PK2_LEN	(IW_EV_LCP_PK2_LEN + sizeof(struct iw_pk_point) - IW_EV_POINT_OFF)
#endif	/* IW_EV_LCP_PK2_LEN */

typedef struct iwscan_state
{
  /* State */
  int			ap_num;		/* Access Point number 1->N */
  int			val_index;	/* Value in table 0->(N-1) */
} iwscan_state;

int scan_active_wifis(const char*, struct sbuf*);

int print_scanning_info(int, const char*, struct sbuf*);

void _iw_print_stats(char*, int, const iwqual*, const iwrange*, int, struct sbuf*);

void _iw_essid_escape(char*, const char*, const int);

#endif /* __WIRELESS_H__ */
