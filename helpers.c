 /*
  * helpers.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */

#ifndef _GNU_SOURCE
   #define _GNU_SOURCE 1
#endif

// #include <sys/wait.h>

#include <proc/readproc.h>

#include "def.h"
#include "helpers.h"
#include "sbuf.h"
#include "interfaces.h"
#include "ipaddr.h"
#include "wireless.h"

#define MAX_SIZE 128
#define ARRAY_DIM 6

void print_active_wifis(const char *wireless_device)
{
   sbuf_t s;
   sbuf_init(&s);   
   scan_active_wifis(wireless_device, &s);
   printf("%s\n", s.buf);
   sbuf_free(&s);
}

short ifquery(const char *ifname)
{
   FILE *fp;
   short state;
   sbuf_t cmd;
      
   sbuf_init(&cmd);
   sbuf_concat(&cmd, 2, "/sbin/ifquery --state ", ifname);
   fp = popen(cmd.buf, "r");
   if(!fp) {
      state=-1;
      goto Free;
   }
   
   state=0;
   if(fgetc(fp)!=EOF) state=1;
   
   pclose(fp);
   
Free:
   sbuf_free(&cmd);
   
   return state;
}

void kill_all_processes()
{
   const char *processes[ARRAY_DIM] = { "dhclient", "dhcpcd", "udhcpc", "pump", "dhcp6c", "wpa_supplicant" };
   
   PROCTAB *proc = openproc(PROC_FILLSTAT);

   while (1) {
	  proc_t *proc_info = NULL;
	  proc_info = readproc(proc, NULL);	
      if(proc_info == NULL) break;
      
      for(unsigned int i=0; i<ARRAY_DIM; i++) {                      
         if(!strcmp(proc_info->cmd, processes[i])) {
            if( (kill(proc_info->tgid, SIGTERM)) != 0 ) kill(proc_info->tgid, SIGKILL);
         }
      }
                  
      freeproc(proc_info);
   }
   
   closeproc(proc);
}
