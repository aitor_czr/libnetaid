/* 
 * netproc.c
 * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
 * 
 * Most of the code of this file is taken from the netstat plug-in of the Lxpanel, 
 * developed by the LxDE team.
 * 
 * simple-netaid is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * simple-netaid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * See the COPYING file.
 */

#include "netproc.h"
#include "sbuf.h"
#include "iproute.h"
#include "def.h"

#include <iwlib.h>

#define NETDEV_STAT_NORMAL	  0
#define NETDEV_STAT_PROBLEM	  1
#define NETDEV_STAT_RENEW	  2
#define NETDEV_STAT_BOTHRS	  3
#define NETDEV_STAT_SENDDATA  4
#define NETDEV_STAT_RECVDATA  5

#define TRUE 1
#define FALSE 0

void
netproc(struct sbuf *s)
{	
	NETDEVLIST_PTR netdevlist=NULL;
	
	FILE *fp = netproc_open();
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);     	  

    int iwsockfd;         /* generic raw socket desc. */
    
	/* Create a channel to the NET kernel. */
	if((iwsockfd = iw_sockets_open()) < 0)
    {
		perror(_("socket"));
		exit(EXIT_FAILURE);
    }
    
    const char *conn_dev = iproute(); 
    
	char buffer[BUFFER_SIZE];
	int count = 0;
	int prx_idx, ptx_idx, brx_idx, btx_idx;
	ulong in_packets, out_packets, in_bytes, out_bytes;
	NETDEVLIST_PTR devptr = NULL;
	// bool _plugged=FALSE;
	bool _wireless;

	/* interface information */
	struct ifreq ifr;
	struct ethtool_test edata;
	iwstats iws;
	char *status;
	char *name;
	struct iw_range iwrange;
	int has_iwrange = 0;

	// Try to read first line from stream
	status = fgets (buffer, BUFFER_SIZE, fp);	
	if (!status)
		printf(_("netstat: netproc_scandevice(): Error reading first line from stream!"));
	
	// Try to read second line from stream	
	status = fgets (buffer, BUFFER_SIZE, fp);	
	if (!status)
		printf(_("netstat: netproc_scandevice(): Error reading second line from stream!"));
		
	netproc_parse_stats_header(buffer, &prx_idx, &ptx_idx, &brx_idx, &btx_idx);

	while (fgets(buffer, BUFFER_SIZE, fp)) {

		/* getting interface name */
		name = buffer;
		while (isspace(name[0])) name++;

		/* reading packet infomation */
		in_packets = out_packets = in_bytes = out_bytes = 0L;
		status = netproc_parse_ifname(name);
		netproc_parse_status(status, prx_idx, ptx_idx, &in_packets, &out_packets,
				     brx_idx, btx_idx, &in_bytes, &out_bytes);

		/* check interface hw_type */
		bzero(&ifr, sizeof(ifr));
		//strncpy(ifr.ifr_name, name, strlen(name));
		strcpy(ifr.ifr_name, name);
		ifr.ifr_name[strlen(name)+1] = '\0';
		if (ioctl(sockfd, SIOCGIFHWADDR, &ifr)<0)
			continue;

		/* hw_types is not Ethernet and PPP */
		if ((ifr.ifr_hwaddr.sa_family != ARPHRD_ETHER) && (ifr.ifr_hwaddr.sa_family!=ARPHRD_PPP))
			continue;

		/* detecting new interface */
		if ((devptr = netproc_netdevlist_find(netdevlist, name))==NULL) {
			
			/* check wireless device */
			has_iwrange = (iw_get_range_info(iwsockfd, name, &iwrange)>=0);
			
			if (!(has_iwrange) || (iwrange.we_version_compiled < 14)) _wireless=FALSE;
			else _wireless=TRUE;
			
			netproc_netdevlist_add(&netdevlist, name, in_bytes, in_packets, out_bytes, out_packets, _wireless);			
			devptr = netproc_netdevlist_find(netdevlist, name);
			
			if(!strcmp(ifr.ifr_name, conn_dev)) {
				sbuf_concat(s, 3, "Connected to ", ifr.ifr_name, "\n"); 
			
				// MAC Address								 
				char mac_addr[BUFFER_SIZE];
				int j=snprintf( mac_addr, BUFFER_SIZE,
								"%02X:%02X:%02X:%02X:%02X:%02X",
								ifr.ifr_hwaddr.sa_data[0] & 0377,
								ifr.ifr_hwaddr.sa_data[1] & 0377,
								ifr.ifr_hwaddr.sa_data[2] & 0377,
								ifr.ifr_hwaddr.sa_data[3] & 0377,
								ifr.ifr_hwaddr.sa_data[4] & 0377,
								ifr.ifr_hwaddr.sa_data[5] & 0377 );
												
				if(j<BUFFER_SIZE) sbuf_concat(s, 3, "MAC Address=", mac_addr, "\n");
			}
				
		} else {
			// Setting device status and update flags 
			if ((devptr->info.recv_packets != in_packets) && (devptr->info.trans_packets != out_packets)) {
				if (devptr->info.status != NETDEV_STAT_BOTHRS)
					devptr->info.updated = TRUE;

				devptr->info.status = NETDEV_STAT_BOTHRS;
			} else if (devptr->info.recv_packets!=in_packets) {
				if (devptr->info.status !=NETDEV_STAT_RECVDATA)
					devptr->info.updated = TRUE;

				devptr->info.status = NETDEV_STAT_RECVDATA;
			} else if (devptr->info.trans_packets!=out_packets) {
				if (devptr->info.status != NETDEV_STAT_SENDDATA)
					devptr->info.updated = TRUE;

				devptr->info.status = NETDEV_STAT_SENDDATA;
			} else {
				if (devptr->info.status != NETDEV_STAT_NORMAL)
					devptr->info.updated = TRUE;

				devptr->info.status = NETDEV_STAT_NORMAL;
			}
						
			/* Recording r/t information */
			devptr->info.recv_bytes = in_bytes;
			devptr->info.recv_packets = in_packets;
			devptr->info.trans_bytes = out_bytes;
			devptr->info.trans_packets = out_packets;

			/* give device a life */
			devptr->info.alive = TRUE;
		}
		
		/* Enable */
		bzero(&ifr, sizeof(ifr));
		strcpy(ifr.ifr_name, devptr->info.ifname);
		ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
		if (ioctl(sockfd, SIOCGIFFLAGS, &ifr)>=0) {
			devptr->info.flags = ifr.ifr_flags;
			if (ifr.ifr_flags & IFF_UP) {
				devptr->info.enable = TRUE;
				devptr->info.updated = TRUE;
			} else {
				devptr->info.enable = FALSE;
				devptr->info.updated = TRUE;
			}
			
			if (devptr->info.enable) {
				/* Workaround for Atheros Cards */
				if (strncmp(devptr->info.ifname, "ath", 3)==0)
					wireless_refresh(iwsockfd, devptr->info.ifname);

				/* plug */
				bzero(&ifr, sizeof(ifr));
				strcpy(ifr.ifr_name, devptr->info.ifname);
				ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
				edata.cmd = 0x0000000a;
				ifr.ifr_data = (caddr_t)&edata;
				if (ioctl(sockfd, SIOCETHTOOL, &ifr)<0) {
					/* using IFF_RUNNING instead due to system doesn't have ethtool or working in non-root */
					if (devptr->info.flags & IFF_RUNNING) {
						if (!devptr->info.plug) {
							devptr->info.plug = true;
							devptr->info.updated = true;
						}
					} else if (devptr->info.plug) {
						devptr->info.plug = false;
						devptr->info.updated = true;
					}
				} else {
					if (*(edata.data)!=0) {
						if (!devptr->info.plug) {
							devptr->info.plug = true;
							devptr->info.updated = true;
						}
					} else if (devptr->info.plug) {
						devptr->info.plug = false;
						devptr->info.updated = true;
					}
				}
				
				/* get network information */
				if (devptr->info.enable&&devptr->info.plug) {
					if (devptr->info.flags & IFF_RUNNING) {	
											
						// if(!_wireless) _plugged=TRUE;
 						
						/* release old information */
						free(devptr->info.ipaddr);
						free(devptr->info.bcast);
						free(devptr->info.mask);
						
						// IP Address 
						bzero(&ifr, sizeof(ifr));
						strcpy(ifr.ifr_name, devptr->info.ifname);
						ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
						if (ioctl(sockfd, SIOCGIFADDR, &ifr)<0)
							devptr->info.ipaddr = strdup("0.0.0.0");							
						else {
							devptr->info.ipaddr = strdup(inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr));
							if(!strcmp(ifr.ifr_name, conn_dev))  sbuf_concat(s, 3, "IP Address=", devptr->info.ipaddr, "\n");
						}

						/* Point-to-Porint Address */
						if (devptr->info.flags & IFF_POINTOPOINT) {
							bzero(&ifr, sizeof(ifr));
							strcpy(ifr.ifr_name, devptr->info.ifname);
							ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
							if (ioctl(sockfd, SIOCGIFDSTADDR, &ifr)<0)
								devptr->info.dest = NULL;
							else
								devptr->info.dest = strdup(inet_ntoa(((struct sockaddr_in*)&ifr.ifr_dstaddr)->sin_addr));
						}

						/* Broadcast */
						if (devptr->info.flags & IFF_BROADCAST) {
							bzero(&ifr, sizeof(ifr));
							strcpy(ifr.ifr_name, devptr->info.ifname);
							ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
							if (ioctl(sockfd, SIOCGIFBRDADDR, &ifr)<0)
								devptr->info.bcast = NULL;
							else
								devptr->info.bcast = strdup(inet_ntoa(((struct sockaddr_in*)&ifr.ifr_broadaddr)->sin_addr));
						}
						if(devptr->info.bcast) {
							if(!strcmp(ifr.ifr_name, conn_dev))  sbuf_concat(s, 3, "BROADCAST=", devptr->info.bcast, "\n");
						}

						/* Netmask */
						bzero(&ifr, sizeof(ifr));
						strcpy(ifr.ifr_name, devptr->info.ifname);
						ifr.ifr_name[IF_NAMESIZE - 1] = '\0';
						if (ioctl(sockfd, SIOCGIFNETMASK, &ifr)<0)
							devptr->info.mask = NULL;
						else
							devptr->info.mask = strdup(inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr));
						if(devptr->info.mask) {
							if(!strcmp(ifr.ifr_name, conn_dev))  sbuf_concat(s, 3, "NETMASK=", devptr->info.mask, "\n");
						}
						
						/* Wireless Information */
						if (devptr->info.wireless) {
							
							struct wireless_config wconfig;

							/* get wireless config */
							if (iw_get_basic_config(iwsockfd, devptr->info.ifname, &wconfig)>=0) {
								
								/* Protocol */
								devptr->info.protocol = strdup(wconfig.name);
								if(!strcmp(ifr.ifr_name, conn_dev))  sbuf_concat(s, 3, "PROTOCOL=", devptr->info.protocol, "\n");								
								
								/* ESSID */
								devptr->info.essid = strdup(wconfig.essid);
								if(!strcmp(ifr.ifr_name, conn_dev))  sbuf_concat(s, 3, "ESSID=", devptr->info.essid, "\n");
								
								/* Signal Quality */
								iw_get_stats(iwsockfd, devptr->info.ifname, &iws, &iwrange, has_iwrange);
								devptr->info.quality = rint((log (iws.qual.qual) / log (92)) * 100.0);
								if(!strcmp(ifr.ifr_name, conn_dev))  {
									sbuf_concat(s, 3, "QUALITY=", itoa__(devptr->info.quality), "%");
								}
							}
						}
						
						/* check problem connection */
						if (strcmp(devptr->info.ipaddr, "0.0.0.0")==0) {
							devptr->info.status = NETDEV_STAT_PROBLEM;
							/* has connection problem  */
							if (devptr->info.connected) {
								devptr->info.connected = FALSE;
								devptr->info.updated = TRUE;
							}
						} else if (!devptr->info.connected) {
								devptr->info.status = NETDEV_STAT_NORMAL;
								devptr->info.connected = TRUE;
								devptr->info.updated = TRUE;
						}
					} else {
						/* has connection problem  */
						devptr->info.status = NETDEV_STAT_PROBLEM;
						if (devptr->info.connected) {
							devptr->info.connected = FALSE;
							devptr->info.updated = TRUE;
						}
					}
				}
			}
		}
		
		devptr = NULL;
		count++;
	} // while

	if(!strcmp(conn_dev, "")) {	
		s->buf[0] = '\0';
		s->bufsiz = 1;
		s->len = 0;
		sbuf_addstr(s, "Disconnected");
	}

	rewind(fp);
	fflush(fp);
		
	netproc_netdevlist_clear(&netdevlist);
	netproc_close(fp);
	iw_sockets_close(iwsockfd);
	close(sockfd);
}

/* network device list */
void
netproc_netdevlist_add(	NETDEVLIST_PTR *netdev_list,
						const char *ifname,
						ulong recv_bytes,
						ulong recv_packets,
						ulong trans_bytes,
						ulong trans_packets,
						bool wireless )
{
	NETDEVLIST_PTR new_dev;
	
	new_dev = (NETDEVLIST*) malloc(sizeof(NETDEVLIST));
	if(!new_dev) {
		printf(_("Memory allocation failure\n"));
		exit(EXIT_FAILURE);
	}

	new_dev->info.ifname = strdup(ifname);

	new_dev->info.mac = NULL;
	new_dev->info.ipaddr = NULL;
	new_dev->info.dest = NULL;
	new_dev->info.bcast = NULL;
	new_dev->info.mask = NULL;
	new_dev->info.alive = TRUE;
	new_dev->info.enable = FALSE;
	new_dev->info.updated = TRUE;
	new_dev->info.plug = TRUE;
	new_dev->info.connected = FALSE;
	new_dev->info.wireless = wireless;
	new_dev->info.status = NETDEV_STAT_NORMAL;
	new_dev->info.recv_bytes = recv_bytes;
	new_dev->info.recv_packets = recv_packets;
	new_dev->info.trans_bytes = trans_bytes;
	new_dev->info.trans_packets = trans_packets;

	new_dev->prev = NULL;
	new_dev->next = *netdev_list;
	if (new_dev->next!=NULL) {
		new_dev->next->prev = new_dev;
	}
		
	*netdev_list = new_dev;
}

void
netproc_netdevlist_destroy( NETDEVLIST_PTR netdev_list )
{
	free(netdev_list->info.ifname);
	free(netdev_list->info.mac);
	free(netdev_list->info.ipaddr);
	free(netdev_list->info.dest);
	free(netdev_list->info.bcast);
	free(netdev_list->info.mask);	
}

void
netproc_netdevlist_clear( NETDEVLIST_PTR *netdev_list )
{
	NETDEVLIST_PTR ptr;
	
	if (*netdev_list==NULL) return;
	
	ptr = *netdev_list;
	while(ptr->next!=NULL) {
		NETDEVLIST_PTR ptr_del;
		ptr_del = ptr;
		ptr = ptr->next;
		netproc_netdevlist_destroy(ptr_del);
		free(ptr_del);
	}

	*netdev_list = NULL;	
}



NETDEVLIST_PTR netproc_netdevlist_find(	NETDEVLIST_PTR netdev_list,
										const char *ifname )
{
	NETDEVLIST_PTR ptr;

	if (netdev_list==NULL)
		return NULL;

	ptr = netdev_list;
	do {

		if (strcmp(ptr->info.ifname, ifname)==0)
			return ptr;

		ptr = ptr->next;
	} while(ptr!=NULL);

	return NULL;
}

char*
netproc_parse_ifname( const char *buffer )
{
	char *ptr;
	if ((ptr = strchr(buffer, ':'))) {
		*ptr++ = '\0';
	}

	return ptr;
}

void
netproc_parse_stats_header(	char *buffer,
							int  *prx_idx,
							int  *ptx_idx,
							int  *brx_idx,
							int  *btx_idx )
{
	char *p;
	int i;

	*prx_idx = *ptx_idx = -1;
	*brx_idx = *btx_idx = -1;

	p = strtok (buffer, "| \t\n");
	p = strtok (NULL, "| \t\n");
	for (i = 0; p; i++, p = strtok (NULL, "| \t\n")) {
		if (!strcmp (p, "packets")) {
			if (*prx_idx == -1)
				*prx_idx = i;
			else
				*ptx_idx = i;
		} else if (!strcmp (p, "bytes")) {
			if (*brx_idx == -1)
				*brx_idx = i;
			else
				*btx_idx = i;
		}
	}
}


bool netproc_parse_status(
		char *buffer,
		int prx_idx,
		int ptx_idx,
		ulong *in_packets,
		ulong *out_packets,
		int brx_idx,
		int btx_idx,
		ulong *in_bytes,
		ulong *out_bytes	
){
	
	char *ptr;
	int i;
	ptr = strtok(buffer, " \t\n");
	for (i=0;ptr;i++, ptr = strtok(NULL, " \t\n")) {
		if (i==prx_idx)
			*in_packets = strtoull(ptr, NULL, 10);
		else if (i==ptx_idx)
			*out_packets = strtoull(ptr, NULL, 10);
		else if (i==brx_idx)
			*in_bytes = strtoull(ptr, NULL, 10);
		else if (i==btx_idx)
			*out_bytes = strtoull(ptr, NULL, 10);
	}

	if (i <= prx_idx || i <= ptx_idx || i <= brx_idx || i <=btx_idx)
		return FALSE;

	return TRUE;
}

FILE*
netproc_open()
{
	FILE *fp;
	
	fp = fopen("/proc/net/dev", "r" );
	if(!fp) {
	    printf(_("Cannot open /proc/net/dev\n"));
	    exit( EXIT_FAILURE );
	}	
	
	return fp;
}

void
netproc_close( FILE *fp )
{
	if(fp) fclose(fp);
}

bool
wireless_refresh (	int iwsockfd,
					const char *ifname )
{
	struct iwreq wrq;

	/* setting interfaces name */
	strncpy(wrq.ifr_name, ifname, IFNAMSIZ);

	return TRUE;
}

char* itoa__(int value)
{
	int count, i, sign;
	char *ptr, *string, *temp; 

	count = 0;
	if ((sign = value) < 0)
    {
		value = -value;
		count++;
    }

	temp = (char *) malloc(INTSIZE + 2);
	if (temp == NULL)
    {
		return(NULL);
    }
	
	memset(temp,'\0', INTSIZE + 2);

	string = (char *) malloc(INTSIZE + 2);
	if (string == NULL)
    {
		return(NULL);
    }
    
	memset(string,'\0', INTSIZE + 2);
	ptr = string;

	do {
		*temp++ = value % 10 + '0';
		count++;
	} while (( value /= 10) >0);

	if (sign < 0) *temp++ = '-';

	*temp-- = '\0';

	for (i = 0; i < count; i++, temp--, ptr++) memcpy(ptr, temp, sizeof(char));
	
	return(string);
}
