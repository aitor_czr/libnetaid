 /*
  * def.h
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#ifndef __DEF_H__
#define __DEF_H__

#ifndef _BSD_SOURCE
#define _DEFAULT_SOURCE 1
#define _BSD_SOURCE
#endif

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

#include "unistd.h"
//#include "util.h"

#define NORMAL_COLOR  "\x1B[0m"
#define GREEN  "\x1B[32m"
#define BLUE  "\x1B[34m"

#define WHERESTR "%05d: [%16s:%04u] %s: "
#define WHEREARG (int)getpid(), __FILE__, __LINE__, __func__

#define log_debug(x, ...) if( DEBUG ) { fprintf(stderr, WHERESTR x "\n", WHEREARG, __VA_ARGS__); }
#define log_error(x, ...) if( DEBUG ) { fprintf(stderr, WHERESTR x "\n", WHEREARG, __VA_ARGS__); }

/*--------------------- Internatioanlization --------------------------------*/
#include <locale.h>
#include <libintl.h>
#define _(StRiNg)  dgettext("libnetaid",StRiNg)

#endif // __DEF_H__

