 /*
  * ipaddr.c
  * Copyright (C) Aitor Cuadrado Zubizarreta <aitor_czr@gnuinos.org>
  * 
  * Most of the code of this file is taken from the RTnetlink service routines
  * of the iproute2 project.
  * 
  * https://git.kernel.org/pub/scm/network/iproute2/iproute2.git/
  * 
  * simple-netaid is free software: you can redistribute it and/or modify it
  * under the terms of the GNU General Public License as published by the
  * Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  * 
  * simple-netaid is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  * See the GNU General Public License for more details.
  * 
  * You should have received a copy of the GNU General Public License along
  * with this program.  If not, see <http://www.gnu.org/licenses/>.
  * 
  * See the COPYING file.
  */


#include <fnmatch.h>

#include "ipaddr.h"
#include "libnetlink.h"
#include "ll_map.h"
#include "sbuf.h"
#include "iproute.h"
#include "def.h"

#define SPRINT_BSIZE 64
#define SPRINT_BUF(x)	char x[SPRINT_BSIZE]

static struct sbuf device;

extern struct rtnl_handle rth;

static struct
{
	int ifindex;
	int family;
	inet_prefix pfx;
	int scope, scopemask;
	int flags, flagmask;
	int up;
	char *label;
	char *flushb;
	int flushed, flushp, flushe;
} filter;

static
int inet_addr_match(const inet_prefix *a, const inet_prefix *b, int bits)
{
   const __u32 *a1 = a->data;
   const __u32 *a2 = b->data;
   int words = bits >> 0x05;

   bits &= 0x1f;

   if (words)
      if (memcmp(a1, a2, words << 2))
         return -1;

   if (bits) {
      __u32 w1, w2;
      __u32 mask;

      w1 = a1[words];
      w2 = a2[words];

      mask = htonl((0xffffffff) << (0x20 - bits));

      if ((w1 ^ w2) & mask)
         return 1;
   }

   return 0;
}

int flush_update(void)
{
	if (rtnl_send_check(&rth, filter.flushb, filter.flushp) < 0) {
		perror(_("Failed to send flush request"));
		return -1;
	}
	filter.flushp = 0;
	return 0;
}

void ipaddr_flush(const char *ifname)
{
	int round = 0;
	char flushb[4096-512];
	
	
	if (rtnl_open(&rth, 0) < 0) exit(1);

	filter.flushb = flushb;
	filter.flushp = 0;
	filter.flushe = sizeof(flushb);
	
			
	while (round < MAX_FLUSH_LOOPS) {
		
		sbuf_init(&device);
		sbuf_addstr(&device, ifname);
		
		const struct rtnl_dump_filter_arg a[3] = {
			{
				.filter = print_addrinfo_secondary,
				.arg1 = stdout,
			},
			{
				.filter = print_addrinfo_primary,
				.arg1 = stdout,
			},
			{
				.filter = NULL,
				.arg1 = NULL,
			},
		};
		
		free(device.buf);
		
		if (rtnl_wilddump_request(&rth, filter.family, RTM_GETADDR) < 0) {
			perror(_("Cannot send dump request"));
			exit(1);
		}
		
		filter.flushed = 0;
	
		if (rtnl_dump_filter_l(&rth, a) < 0) {
			fprintf(stderr, _("Flush terminated\n"));
			exit(1);
		}
		
		if (filter.flushed == 0) {
 flush_done:			
	
			fflush(stdout);
			return;
		}
		
		round++;
		
		if (flush_update() < 0) return;
		
		if (!(filter.flags & IFA_F_SECONDARY) && (filter.flagmask & IFA_F_SECONDARY))
			goto flush_done;
	}
	
	fprintf(stderr, _("*** Flush remains incomplete after %d rounds. ***\n"), MAX_FLUSH_LOOPS);
	fflush(stderr);
	
	fflush(stdout);
}

int print_addrinfo(const struct sockaddr_nl *who, struct nlmsghdr *n, void *arg)
{
	struct ifaddrmsg *ifa = NLMSG_DATA(n);
	int len = n->nlmsg_len;
	/* Use local copy of ifa_flags to not interfere with filtering code */
	unsigned int ifa_flags;
	struct rtattr * rta_tb[IFA_MAX+1];
	
	if (n->nlmsg_type != RTM_NEWADDR && n->nlmsg_type != RTM_DELADDR) 
		return 0;		
		
	len -= NLMSG_LENGTH(sizeof(*ifa));
	if (len < 0) {
		fprintf(stderr, _("BUG: wrong nlmsg len %d\n"), len);
		return -1;
	}

	if (filter.flushb && n->nlmsg_type != RTM_NEWADDR)
		return 0;

	parse_rtattr(rta_tb, IFA_MAX, IFA_RTA(ifa),
		     n->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa)));

	ifa_flags = get_ifa_flags(ifa, rta_tb[IFA_FLAGS]);

	if (!rta_tb[IFA_LOCAL])
		rta_tb[IFA_LOCAL] = rta_tb[IFA_ADDRESS];
	if (!rta_tb[IFA_ADDRESS])
		rta_tb[IFA_ADDRESS] = rta_tb[IFA_LOCAL];

	if (filter.ifindex && filter.ifindex != ifa->ifa_index)
		return 0;
	if ((filter.scope^ifa->ifa_scope)&filter.scopemask)
		return 0;
	if ((filter.flags ^ ifa_flags) & filter.flagmask)
		return 0;
	if (filter.label) {
		SPRINT_BUF(b1);
		const char *label;
		if (rta_tb[IFA_LABEL])
			label = RTA_DATA(rta_tb[IFA_LABEL]);
		else
			label = ll_idx_n2a(ifa->ifa_index, b1);
		if (fnmatch(filter.label, label, 0) != 0)
			return 0;
	}
	if (filter.pfx.family) {
		if (rta_tb[IFA_LOCAL]) {
			inet_prefix dst;
			memset(&dst, 0, sizeof(dst));
			dst.family = ifa->ifa_family;
			memcpy(&dst.data, RTA_DATA(rta_tb[IFA_LOCAL]), RTA_PAYLOAD(rta_tb[IFA_LOCAL]));
			if (inet_addr_match(&dst, &filter.pfx, filter.pfx.bitlen))
				return 0;
		}
	}

	if (filter.family && filter.family != ifa->ifa_family)
		return 0;

	if (filter.flushb) {
		struct nlmsghdr *fn;
		if (NLMSG_ALIGN(filter.flushp) + n->nlmsg_len > filter.flushe) {
			if (flush_update())
				return -1;
		}
		fn = (struct nlmsghdr*)(filter.flushb + NLMSG_ALIGN(filter.flushp));
		memcpy(fn, n, n->nlmsg_len);
		fn->nlmsg_type = RTM_DELADDR;
		fn->nlmsg_flags = NLM_F_REQUEST;
		fn->nlmsg_seq = ++rth.seq;
		filter.flushp = (((char*)fn) + n->nlmsg_len) - filter.flushb;
		filter.flushed++;
	}
	
	return 0;
}

unsigned int get_ifa_flags(struct ifaddrmsg *ifa,
				  struct rtattr *ifa_flags_attr)
{
	return ifa_flags_attr ? rta_getattr_u32(ifa_flags_attr) :
				ifa->ifa_flags;
}

int print_addrinfo_primary(const struct sockaddr_nl *who,
				  struct nlmsghdr *n, void *arg)
{		
	struct ifaddrmsg *ifa = NLMSG_DATA(n);
	
	if(strcmp(device.buf, ll_index_to_name(ifa->ifa_index))) return 0;

	if (ifa->ifa_flags & IFA_F_SECONDARY)
		return 0;

	return print_addrinfo(who, n, arg);
}

int print_addrinfo_secondary(const struct sockaddr_nl *who,
				    struct nlmsghdr *n, void *arg)
{		
	struct ifaddrmsg *ifa = NLMSG_DATA(n);
	
	if(strcmp(device.buf, ll_index_to_name(ifa->ifa_index))) return 0;

	if (!(ifa->ifa_flags & IFA_F_SECONDARY)) return 0;

	return print_addrinfo(who, n, arg);
}



