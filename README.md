# libnetaid

libnetaid is a shared C library based on the RTnetlink service routines of the linux kernel. 
It provides useful utilities focused to the development of both CLI and GUI user interfaces 
to render networking management with minimal dependencies and following the KISS principle, 
together with other tools like wpasupplicant, ifupdown and libiw.


## Installation via APT

Use the [APT](https://wiki.debian.org/Apt) package manager to install libnetaid.

```bash
apt-get install libnetaid libnetaid-dev
```

After that, development headers will be located in:

```bash
/usr/include/i386-linux-gnu/simple-netaid    (for 32 bits) 
/usr/include/x86_64-linux-gnu/simple-netaid  (for 64 bits)
```

### Building your own packages

You can build your own debian packages in a very easy way using `git-buildpackage` (a wrapper for `dpkg-buildpackage`). 
All you need to do is:

```sh
$ git clone https://git.devuan.org/aitor_czr/libnetaid.git
$ cd libnetaid
```

If you type `git status` you can see all:

```sh
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working directory clean
```

To list all the local and remote branches type:

```sh
$ git branch -a
```

Let's checkout on the following branches (in this order):

```sh
$ git checkout -b pristine-tar origin/pristine-tar
$ git checkout -b gbp-release_0.1 origin/gbp-release_0.1
```

The prefix `gbp` reflects those branches containing the debian folder. 
Now we can build the packages:

```sh
$ gbp buildpackage
```

Once the packages have been generated, go to the build directory and install them:

```sh
$ cd ../build-area
$ sudo dpkg -i *.deb
```


## Building from source

> **Note**: This paragraph needs to be updated, since cmake has been replaced with autotools in the master branch. 

If you are using a different package management system, then you'll need to build the project from source.

### Build dependencies

The current version of libnetaid has been tested under Devuan Beowulf, requering:
- `gcc (4:8.3.0-1)`
- `cmake (3.13.4-1)`
- `libiw-dev (30~pre9-13)`

### Procedure

Download the tarball from the git repository of devuan [libnetaid](https://git.devuan.org/aitor_czr/libnetaid/archive/master.tar.gz), 
extract it with `tar xvf master.tar.gz` and go into the extracted folder. There, run the 
following commands: 

```sh
$ mkdir build
$ cd build
$ cmake ..
$ make
$ sudo make install
```
> **Note**: By default, CMake does not provide the "make uninstall" target; so, in general, you cannot do this. 
However, libnetaid adds an uninstall target to the CMAKE generated Makefile, in such a way that you can run:

```sh
$ sudo make uninstall
```

You can test libnetaid trying to build one of the examples bellow or the [snetaid](https://git.devuan.org/aitor_czr/snetaid) daemon. 
If the library in `/usr/local/lib` is not found (this is the destination when building from source), what you need to do is to tell 
the runtime loader to also look in this directory for libraries. There are two ways for that:

- The first way is to add the path to the `LD_LIBRARY_PATH` environment variable:

```sh
      export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/lib"
```

- The second way is to update the configuration file of the runtime linker. This can happen either in the `/etc/ld.so.conf` file, by 
putting the line `/usr/local/lib` somewhere in that file, or by creating a new *.conf file in the `/etc/ld.so.conf.d/` directory that 
contains the new path. For example `/etc/ld.so.conf.d/99local.conf` with just `/usr/local/lib` in it. This is the recommended way of 
doing this, as it allows you to keep your custom library paths separate from paths set by the system. The "99" prefix is there to 
make sure the file is loaded last compared to other files there, so that it won't preempt system paths that could contain the same 
libraries (read [here](https://stackoverflow.com/questions/17889799/libraries-in-usr-local-lib-not-found)). After you modify/create 
the file in `/etc`, you need to update the `/etc/ld.so.cache` file:

```sh
      $ sudo ldconfig
```


### Additional dependencies

To get all the features of libnetaid working you'll need to install some additional packages, they are:
- `wpasupplicant`
- `ifupdown`

A typical configuration of `/etc/network/interfaces` might be:

```sh
source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

allow-hotplug eth0
iface eth0 inet dhcp

allow-hotplug wlan0
iface wlan0 inet dhcp
```

Replace here the names of the network devices according to your system.

You need to restart the service for the changes to take effect:

```sh
sudo service networking restart
```


## Usage

libnetaid uses sbuf structs to handle char arrays, whose life cycle is (have a look at the [sbuf.c](https://git.devuan.org/aitor_czr/libnetaid/src/branch/master/backend_src/sbuf.c) file):

```c
struct sbuf s;
sbuf_init(&s);
sbuf_addch(&s, 'F');     /* adds a character */
sbuf_addstr(&s, "oo");   /* adds an array */
free(s.buf);
```

The following example consists of a very simple C program showing the names of the network interfaces:

```c
/ * *
  *
  *  main.c
  *
  */

#include <simple-netaid/sbuf.h>
#include <simple-netaid/interfaces.h>

int main(int argc, char **argv)
{
	struct sbuf wired_device, wireless_device;
	sbuf_init(&wired_device);
	sbuf_init(&wireless_device);	
	get_interfaces(&wired_device, &wireless_device);	
	printf("Wired device: %s\n", wired_device.buf);
	printf("Wireless device: %s\n", wireless_device.buf);	
	free(wired_device.buf);
	free(wireless_device.buf);
	return 0;
}
```

You can build it as follows:

```bash
gcc main.c -o main -lnetaid
```

Output example:

```bash
Wired device: eth0
Wireless device: wlan0
```

Another example using netproc might be:


```c
/ * *
  *
  *  main.c
  *
  */
 
#include <simple-netaid/sbuf.h>
#include <simple-netaid/netproc.h>

int main( int argc, char **argv)
{
	struct sbuf s;
	sbuf_init(&s);
	netproc(&s);
	printf("%s\n", s.buf);
	free(s.buf);
	return 0;
}
```

Output example:

```bash
Connected to wlan0
MAC Address=2C:D0:5A:E8:9E:9F
IP Address=192.168.0.12
BROADCAST=192.168.0.255
NETMASK=255.255.255.0
PROTOCOL=IEEE 802.11
ESSID=Euskaltel-58YA
QUALITY=94%
```

## Functions

Here you are a list of other available functionalities:

```c
// Brings up the interface (analogous to "ip link set <interface> up")
void interface_up (const char* interface); 
```

```c
// Brings down the interface (analogous to "ip link set <interface> down")
void interface_down (const char* interface);
```

```c
// Returns the current status of the interface ( 1 | 0 ) -> ( UP | DOWN )
int get_interface_status (const char* interface);
```

```c
// Brings up the interface (analogous to "ifup <interface>")
void ifup (const char* interface);
```

```c
// Brings down the interface (analogous to "ifdown <interface>")
void ifdown (const char* interface);
```

```c
// Shows all the available active wifis (as root)
void print_active_wifis(const char* wireless_device);
```

```c
// Wireless connection attempt via wpa_supplicant
void wireless_connection(const char *ifname, const char *essid, const char *password);
```

```c
// Wired connection attempt
void wired_connection(const char* wired_device);
```
	
```c
// Disconnects the network device
void disconnect(const char* device);
```
	
```c
// Returns the name of the connected network device, null if you are disconnected.
char* iproute();
```

```c
// Asigns to the buffer detailed information concerning to the network connection.
void netproc (struct sbuf* buffer);
```


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what 
you would like to change.

Please make sure to update tests as appropriate.


## License

libnetaid is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, either version 3 
of the License, or (at your option) any later version.

libnetaid is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. 
If not, see <http://www.gnu.org/licenses/>.

See the COPYING file.
