ROOT_DIR := $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))")
BUILD    ?= $(ROOT_DIR)/build

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH 2>/dev/null || true)

PREFIX ?= /usr/local
DESTDIR ?= /
INCLUDE_PREFIX ?= $(PREFIX)

CC	    ?= cc
CFLAGS ?= -Wall -std=gnu99 -D_GNU_SOURCE -g -fPIC
LIBINC   := -I$(BUILD)

NETAID_SRCS := $(wildcard *.c)

HEADERS := ethlink.h helpers.h interfaces.h ipaddr.h iproute.h libnetlink.h ll_map.h \
	netproc.h sbuf.h wireless.h

NETAID_OBJ   := $(patsubst %.c,$(BUILD)/%.o,$(NETAID_SRCS))

VERSION := 1.3

VERSION_MAJOR := 1
VERSION_MINOR := 0
VERSION_PATCH := 1

NETAID := libnetaid.so
NETAID_SO := libnetaid.so.$(VERSION_MAJOR)
NETAID_LIB := libnetaid.so.$(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH)

BINDIR ?= $(DESTDIR)/$(PREFIX)/bin
LIBDIR ?= $(DESTDIR)$(PREFIX)/lib/$(DEB_HOST_MULTIARCH)
INCLUDE_DIR ?= $(DESTDIR)$(INCLUDE_PREFIX)/include/simple-netaid
PKGCONFIGDIR ?= $(DESTDIR)$(PREFIX)/lib/$(DEB_HOST_MULTIARCH)/pkgconfig

BUILD_NETAID_LIB := $(patsubst %,$(BUILD)$(LIBDIR)/%,$(NETAID_LIB))
INSTALL_NETAID_LIB := $(patsubst $(BUILD)$(LIBDIR)/%,$(LIBDIR)/%,$(BUILD_NETAID_LIB))

BUILD_NETAID_SO := $(patsubst %,$(BUILD)$(LIBDIR)/%,$(NETAID_SO))
INSTALL_NETAID_SO := $(patsubst $(BUILD)$(LIBDIR)/%,$(LIBDIR)/%,$(BUILD_NETAID_SO))

BUILD_NETAID := $(patsubst %,$(BUILD)$(LIBDIR)/%,$(NETAID))
INSTALL_NETAID := $(patsubst $(BUILD)$(LIBDIR)/%,$(LIBDIR)/%,$(BUILD_NETAID))

BUILD_HEADERS := $(patsubst %,$(BUILD)/$(INCLUDE_DIR)/%,$(HEADERS))
INSTALL_HEADERS := $(patsubst $(BUILD)/$(INCLUDE_DIR)/%,$(INCLUDE_DIR)/%,$(BUILD_HEADERS))

BUILD_PC_FILE := $(BUILD)$(PKGCONFIGDIR)/libnetaid.pc
INSTALL_PC_FILE := $(PKGCONFIGDIR)/libnetaid.pc

all: libs

libs: $(BUILD_HEADERS) $(BUILD_PC_FILE) $(BUILD_NETAID) $(BUILD_NETAID_SO) $(BUILD_NETAID_LIB)

$(BUILD_PC_FILE): libnetaid.pc.in
	@mkdir -p "$(shell dirname "$@")"
	@cat $< | \
		sed -e 's~@prefix@~$(PREFIX)~g;' | \
		sed -e 's~@includedir@~$(INCLUDE_DIR)~g;' | \
		sed -e 's~@version@~$(VERSION)~g; ' | \
		sed -e 's~@libdir@~$(LIBDIR)~g; ' > $@

$(BUILD_NETAID_LIB): $(NETAID_OBJ)
	@mkdir -p "$(shell dirname "$@")"
	$(CC) $(CFLAGS) -shared -Wl,-soname,$(NETAID_SO) -o "$@" $(NETAID_OBJ) $(LIBINC) -lm -liw -lprocps

$(BUILD_NETAID_SO): $(BUILD_NETAID_LIB)
	@mkdir -p "$(shell dirname "$@")"
	@ln -sf "$(shell basename "$<")" "$@"

$(BUILD_NETAID): $(BUILD_NETAID_SO)
	@mkdir -p "$(shell dirname "$@")"
	@ln -sf "$(shell basename "$<")" "$@"

$(BUILD)/$(INCLUDE_DIR)/%.h: %.h
	@mkdir -p "$(shell dirname "$@")"
	@cp -a "$<" "$@"

$(INSTALL_NETAID): $(BUILD_NETAID)
	@mkdir -p "$(shell dirname "$@")"
	cp -a "$<" "$@"

$(INSTALL_NETAID_SO): $(BUILD_NETAID_SO)
	@mkdir -p "$(shell dirname "$@")"
	cp -a "$<" "$@"

$(INSTALL_NETAID_LIB): $(BUILD_NETAID_LIB)
	@mkdir -p "$(shell dirname "$@")"
	cp -a "$<" "$@"

$(INCLUDE_DIR)/%.h: $(BUILD)/$(INCLUDE_DIR)/%.h
	@mkdir -p "$(shell dirname "$@")"
	cp -a "$<" "$@"

$(INSTALL_PC_FILE): $(BUILD_PC_FILE)
	@mkdir -p "$(shell dirname "$@")"
	cp -a "$<" "$@"

libs-install: $(INSTALL_NETAID) $(INSTALL_NETAID_SO) $(INSTALL_NETAID_LIB) \
	$(INSTALL_PC_FILE)

headers-install: $(INSTALL_HEADERS)

install: libs-install headers-install

$(BUILD)/%.o : %.c
	@mkdir -p "$(shell dirname "$@")"
	$(CC) $(CFLAGS) -o "$@" $(LIBINC) -c "$<"

.PHONY: clean
clean:
	rm -f $(BUILD_HEADERS) $(BUILD_NETAID_LIB) $(BUILD_NETAID_SO) $(BUILD_NETAID) \
		$(BUILD_PC_FILE) $(BUILD_HEADERS) $(NETAID_OBJ)

.PHONY: uninstall 
uninstall:
	rm -f $(INSTALL_HEADERS) $(INSTALL_NETAID_LIB) $(INSTALL_NETAID_SO) $(INSTALL_NETAID) \
		$(INSTALL_PC_FILE)

print-%: ; @echo $*=$($*)

